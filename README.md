# Gentoocfg

Gentoo scripts and configs

## Description

This repo consists of *gentoo linux installation* scripts and *configuration* files.

### Usage

After Entering CHROOT ENVIRONMENT run the following commands

```
curl -sO https://gitlab.com/larnix/gentoocfg/raw/master/gentoo.script
chmod +x gentoo.script
./gentoo.script
```

### Result

Single script handles everything from installing kernel to setting up desktop environment (suckless software)

[under development ...]
